cat >> ~/.bashrc << HEREDOC

# Define these:
# alias magento='php ~/path/to/executable'
# export PROJECTNAME=~/path/to/project
alias mkdir='mkdir -p'

alias tree='tree --dirsfirst'
alias t='tree'

alias hi='highlight -O ansi --force'

alias rp='realpath'

alias r='ranger'
HEREDOC

cat >> ~/.inputrc << HEREDOC

set editing-mode vi
set completion-ignore-case on
HEREDOC

cat >> ~/.vimrc << "HEREDOC"

let mapleader = "\<tab>"

"highlight and replace all occurences of the word under cursor
nnoremap <leader>r /<C-r><C-w><Enter>N<Esc>:%s/<C-r><C-w>//g<Bar>normal<Space>``<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>

"bring up a pre-typed replace-all command which will return you to your original place afterwards
nnoremap <leader>s :%s///g<Bar>normal<Space>``<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3

filetype plugin indent on
syntax enable

set autoindent
set number relativenumber
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set scrolloff=999

let g:closetag_filenames = '*.html*,*.xhtml*,*.phtml*,*.xml*'

colorscheme gruvbox
set background=dark

"Open new files in insert mode
autocmd BufNewFile * startinsert
autocmd Filetype php setlocal tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype xml setlocal tabstop=4 shiftwidth=4 softtabstop=4
HEREDOC
