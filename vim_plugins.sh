#!/usr/bin/env bash

#ensure the native vim plugins directory exists
mkdir -p $HOME/.vim/pack/plugins
plugins=$HOME/.vim/pack/plugins

#remove existing plugin copies, download latest versions, and delete the .git folders

# adds simple HTML/XML tag auto-closing
rm -rf $plugins/start/closetag/{*,\.git*}
git clone --depth=1 https://github.com/alvan/vim-closetag.git $plugins/start/closetag
rm -rf !$/.git*

# adds intelligent handling for brackets, braces, and parentheses
rm -rf $plugins/start/auto-pairs/{*,\.git*}
git clone --depth=1 https://github.com/jiangmiao/auto-pairs.git $plugins/start/auto-pairs
rm -rf !$/.git*

# adds powerful vim-like bindings for adding or modifying brackets in place
rm -rf $plugins/start/surround/{*,\.git*}
git clone --depth=1 https://github.com/tpope/vim-surround.git $plugins/start/surround
rm -rf !$/.git*

# allows dot-key repeat-last support for many non-native commands
rm -rf $plugins/start/repeat/{*,\.git*}
git clone --depth=1 https://github.com/tpope/vim-repeat.git $plugins/start/repeat
rm -rf !$/.git*

# adds per-file autocomplete based only on existing words
rm -rf $plugins/start/supertab/{*,\.git*}
git clone --depth=1 https://github.com/ervandew/supertab.git $plugins/start/supertab
rm -rf !$/.git*
